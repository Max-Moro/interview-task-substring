# Поиск подстроки

Задание для собеседования на Scala Middle разработчика на стеке Akka Platform.

## Общая формулировка задачи

```
Строка:   fligdkmtmgdldkgkkfjsivjoqqxasoskvprfejrgkrlidgkdssadkudfglidgkmashdua
Подстрока:  ligdkm
```

**Задача:** Найти количество вхождений подстроки в строку, с учетом всех возможных перестановок символов в подстроке.

## Формализованные кейсы

```scala
  "Базовый случай" in {
    finder.find("fligdkmtmgdldkgkkfjsivjoqqxasoskvprfejrgkrlidgkdssadkudfglidgkmashdua", "ligdkm").futureValue should be (Seq(
      FoundSubstring("ligdkm", 1),
      FoundSubstring("lidgkm", 57)
    ))
  }

  "Очень длинная исходная строка" in {
    val complexityN = 1000000

    val sb = new StringBuilder
    (0 until complexityN) foreach { _ =>
      sb.append("fligdkmtmgdldkgkkfjsivjoqqxasoskvprfejrgkrlidgkdssadkudfglidgkmashdua")
    }

    finder.find(sb.toString, "ligdkm").futureValue(longTimeout) should be (
      (0 until complexityN).map(_ * 69).flatMap { offset =>
        Seq(
          FoundSubstring("ligdkm", offset + 1),
          FoundSubstring("lidgkm", offset + 57)
        )
      }
    )
  }

  "Очень длинная подстрока для поиска" in {
    val complexityN = 5000

    val sbSubstring = new StringBuilder
    (0 until complexityN) foreach { _ =>
      sbSubstring.append("ligdkm")
    }
    val substring = sbSubstring.toString

    val permutations = substring.permutations
    val sub1 = permutations.next()
    val sub2 = permutations.next()

    val sb = new StringBuilder
    sb.append("f")
    sb.append(sub1)
    sb.append("tmgdldkgkkfjsivjoqqxasoskvprfejrgkrlidgkdssadkudfg")
    sb.append(sub2)
    sb.append("ashdua")

    finder.find(sb.toString, substring).futureValue(longTimeout) should be(Seq(
      FoundSubstring(sub1, 1),
      FoundSubstring(sub2, 1 + (complexityN * 6) + 50)
    ))
  }
```

## Требуемый порядок решения

1. Форкнуть данный репозиторий в свои персональные проекты.
2. Создать проект для последней версии `Scala 2.13.x` и последней версии `SBT`.
3. Подключить библиотеку `ScalaTest` и перенести в нее формализованные кейсы.
4. Создать и протестировать максимально короткую и выразительную реализацию с логарифмической сложностью.
5. Создать и протестировать реализацию с линейной сложностью.
6. Создать и протестировать реализацию, задействующую все вычислительные ядра процессора. В решении использовать последнюю версию библиотеки `Akka Streams`.

## Что оценивается

Каждый шаг выполнения должен быть оформлен в виде отдельного комита и сразу по готовности отправлен на проверку.

Итоговая история репозитория должны выглядеть следующим образом:

![](./doc/final-repository-history.png)

Итоговый набор запускаемых тестов должен выглядеть примерно следующим образом:

![](./doc/final-set-of-tests.png)

Сделать выводы и объяснить полученные результаты времени работы тестов с точки зрения теории сложности алгоритмов и особенностей параллельного выполнения логики в Akka-стримах.